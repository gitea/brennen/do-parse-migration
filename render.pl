#!/usr/bin/env perl

use strict;
use warnings;
use 5.10.0;

use Text::Markdown::Discount;

# Enable html5 block-level tags:
Text::Markdown::Discount::with_html5_tags();
my $flags = Text::Markdown::Discount::MKD_EXTRA_FOOTNOTE();
my $markdown = Text::Markdown::Discount->new;

my $full_source = '';
while (my $source = get_input()) {
  $full_source .= $source;
}

print replace_some_stuff($markdown->markdown($full_source, $flags));

sub get_input {
  local $/ = undef;
  my $source = <>;
  return $source;
}

# Super cheeseball, man.
sub replace_some_stuff {
  my ($markup) = @_;
  $markup =~ s{&lt;\^&gt;(.*?)&lt;\^&gt;}{<span style="color: red;">$1</span>}g;
  $markup =~ s{\[label (.*?)\]}{<strong>$1</strong><br>}g;
  $markup =~ s{\[secondary_label (.*?)\]}{<span style="color: gray;">$1</span><br>}g;
  return $markup;
}
